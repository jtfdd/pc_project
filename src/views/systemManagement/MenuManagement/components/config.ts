import {GetMenuSub} from "@/api/systemManagement/MenuManagement"
export const copyFrom={
    id:'',
    name:"",
    postCode: "",
    status:false,
    description:"",
}
// 三级联动地址
export const cascaderProps = {
   checkStrictly:true,
  lazy: true,
  emitPath:false,
  lazyLoad(node:any, resolve:any) {
    const { level, value } = node;
    GetMenuSub({
      id: !level ?"0": value,
    }).then((res:any) => {
      if (res.code == 200) {
        let arr = res.data.map((item:any) => {
          return {
            value: item.id,
            label: item.name
          };
        });
        resolve(arr);
      }
    });
  },
}