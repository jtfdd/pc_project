
const that = this
// 防抖
export function debounce (fn:any, wait = 1000){
    let time:any = null;
    return function () {
        time && clearTimeout(time);
        time = setTimeout(function () {
            fn.apply(that,arguments)
           }, wait)
     }
  }
// 深拷贝
export const deepClone=(val:any)=>JSON.parse(JSON.stringify(val))
// 列表栏宽度=左1px+边距4px+12*N(字数) px+边距4px+右1px
// n:字数

export const getColWidth =(n:number)=> {
    return 12 * Math.max(3, n) + 14;  
}
// 5.1）操作栏宽度计算规则（X：按钮个数；N：字体个数）：
// 操作栏宽度=左表格线1px+边距4px+按钮宽度（边距12px+12*N px+边距12px）+按钮与按钮间距(X-1)*12px+边距4px+右表格线1px
export const getBtnColWidth = (x:number, n:number) => {
    return  n * 12 + 16 * x + 10;
 }
 const baseColWidth:any = {};
for (let i = 2; i <= 30; i++) {
   baseColWidth['word' + i] = getColWidth(i);
}
const _EColWidth = {
    ...baseColWidth,
    no: 40,  //排序编号
    amount: 125, //金额栏：125
    remark: 300, //最小-备注：300
    deptName: baseColWidth.word6, //部门名称-8字-106px
    fileName: 200, //文件名
    companyName: 200, //公司名-固定，列少时加min-
    companyNameMin: baseColWidth.word6, //公司名简称-固定，列少时加min-
    name: baseColWidth.word4, // 人名-4字-58px
    email: 200, //邮箱
    phone: 85,  //手机号
    date: 72,  //日期
    datetime: 150,  //日期时间
    bankName: 300, //银行名称-固定，列少时加min-
    processStatus: baseColWidth.word4, // 业务管理流程状态-4字-58px
    selectWidth:40,//多选框
}

export const EColWidth = _EColWidth;

/**
 * 
 * @param {*} param0 isMinWidth 是否是有最小宽度属性 width 宽度
 * @returns 
 */

export const hasMinWidth=({isMinWidth,width}:{isMinWidth:boolean;width:Number})=>{
    let obj:any={}
    isMinWidth?obj["min-width"]=width:obj["width"]=width
    return obj
 }
//  公共状态
 export const statusList = [
    {
      label: "启用",
      value: 0,
    },
    {
      label: "禁用",
      value: 1,
    },
  ];

 
  

   