import {ElLoading} from 'element-plus';
export const useLoad=()=>{
    interface Loading {
        close: () => void;
    }
    let loading:Loading|null=null;
    const loadingStart=(title="保存中...")=>{
        loading = ElLoading.service({
            lock: true,
            text: title,
            background: 'rgba(0, 0, 0, 0.7)',
        });
    }
    const  loadingClose=()=>{
        loading&&loading.close()
    }
    return {
        loadingStart,
        loadingClose
    }
}
