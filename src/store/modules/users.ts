
import { defineStore} from 'pinia';
 
export const useUserStore = defineStore({
  id: 'useUserStore',
  state: () => ({
    menuRoutes:[
        {
            path: '/home',
            name: 'home',
            component:()=>import("@/views/home/index.vue"),
            meta:{
                icon:"icon-zhuye",
                title:"首页",
                show:true
            }
        },
        {
            path: '/systemManagement',
            name: 'systemManagement',
            // component:()=>import("@/views/home/index.vue"),
            meta:{
                icon:"icon-xitongguanli",
                title:"系统管理",
                show:true
            },
            children:[
                {
                    path: '/employeeManagement',
                    name: 'employeeManagement',
                    component:()=>import("@/views/systemManagement/personnelManagement/index.vue"),
                    meta:{
                        // icon:"icon-zhuye",
                        title:"员工管理",
                        show:true
                    },
                },
                {
                    path: '/menuManagement',
                    name: 'menuManagement',
                    component:()=>import("@/views/systemManagement/MenuManagement/index.vue"),
                    meta:{
                        // icon:"icon-zhuye",
                        title:"菜单管理",
                        show:true
                    },
                },
                {
                    path: '/roleManagement',
                    name: 'roleManagement',
                    component:()=>import("@/views/systemManagement/roleManagement/index.vue"),
                    meta:{
                        // icon:"icon-zhuye",
                        title:"角色管理",
                        show:true
                    },
                },

            ]
        }
    ]
  }),
  actions: {
    handleCollapse(){
        console.log("进来了")
        this.collapse=!this.collapse
    }
    // your actions
  },
});