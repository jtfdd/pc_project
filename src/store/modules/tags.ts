
import { defineStore} from 'pinia';
 
export const useTagsStore = defineStore({
  id: 'useTagsStore',
  state: () => ({
    curTagPath:"",
    routeTag:[],
    homeRoute:{"name":"home","path":"/", meta: { title: '首页' }}
    
  }),
  actions: {
    //添加页签
    addTag(obj) {
        if(this.routeTag.length == 0 && obj.name !== this.homeRoute.name){
          this.routeTag.push(this.homeRoute);
        }
        let index = this.routeTag.findIndex(f => f.path == obj.path);
        if (index >= 0) {
          this.curTagPath = this.routeTag[index].path;
          this.routeTag.splice(index, 1, obj)
        }
        else {
          this.curTagPath = obj.path;
          this.routeTag.push(obj);
        }
      },
       //关闭页签
    closeTag(obj) {
        return new Promise((resolve) => {
            let index = this.routeTag.findIndex(f => f.path == obj.path);
             this.routeTag.splice(index, 1);
            resolve();
        })
       
      },
//关闭所有页签
closeAllTag() {
    return new Promise((resolve) => {
        this.routeTag =[]
        resolve();
    })
    
  },
 //关闭其他页签
 closeOtherTag() {
    return new Promise((resolve) => {
        this.routeTag = this.routeTag.filter(item => item.path === this.curTagPath || item.name === this.homeRoute.name);
        resolve();
    })
    
  },

        
  },
});