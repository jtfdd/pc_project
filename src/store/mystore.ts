
// src/store/myStore.js
import { defineStore} from 'pinia';
 
export const useMyStore = defineStore({
  id: 'myStore',
  state: () => ({
    num:"123"
    // your state properties
  }),
  actions: {
    // your actions
  },
});