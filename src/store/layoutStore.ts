
import { defineStore} from 'pinia';
 
export const useLayoutStore = defineStore({
  id: 'layoutStore',
  state: () => ({
    collapse:false
    // your state properties
  }),
  actions: {
    handleCollapse(){
        console.log("进来了")
        this.collapse=!this.collapse
    }
    // your actions
  },
});