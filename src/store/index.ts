import { defineStore } from "pinia"
import {EmployeeInfo} from "@/api/systemManagement/EmployeeManagement"
import { ResultData } from "@/api/common/R";
import {employee} from "@/api/systemManagement/EmployeeManagement/types"

export const useLoginStore=defineStore({
  id:"loginStore",
  state:()=>({
    token:"",
    userInfo:{},
    menuList:[],
  }),
  actions:{
   async setLoginUserInfo(){
        EmployeeInfo().then((res:ResultData<employee.Employee>)=>{
            if(res.code===200){
                this.userInfo=res.data
            }
        })
    },
    setMenuList(){

    },
     setToken(token:string){
      this.token=token
    },
  },
  getters:{
    getToken():string{
      return this.token
    },
    getUserInfo():any{
      return this.userInfo
    }
  }
})