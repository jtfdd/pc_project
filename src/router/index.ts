
import { createRouter, createWebHashHistory, RouteRecordRaw } from 'vue-router';
const routes: Array<RouteRecordRaw> = [
    {
        path: '/',
        name: 'layout',
        component:()=>import("@/components/layout/index.vue"),
        meta: {
            title: '首页',
            show: true,
            icon: 'House',
          },
        redirect: '/home',
        children: [
            {
                path: '/home',
                name: 'home',
                component:()=>import("@/views/home/index.vue")
            },
            {
                path: '/systemManagement',
                name: 'systemManagement',
                // component:()=>import("@/views/home/index.vue"),
                meta:{
                    icon:"icon-xitongguanli",
                    title:"系统管理",
                    show:true
                },
                children:[
                    {
                        path: '/employeeManagement',
                        name: 'employeeManagement',
                        component:()=>import("@/views/systemManagement/personnelManagement/index.vue"),
                        meta:{
                            // icon:"icon-zhuye",
                            title:"员工管理",
                            show:true
                        },
                    },
                    {
                        path: '/menuManagement',
                        name: 'menuManagement',
                        component:()=>import("@/views/systemManagement/MenuManagement/index.vue"),
                        meta:{
                            // icon:"icon-zhuye",
                            title:"菜单管理",
                            show:true
                        },
                    },
                    {
                        path: '/roleManagement',
                        name: 'roleManagement',
                        component:()=>import("@/views/systemManagement/roleManagement/index.vue"),
                        meta:{
                            // icon:"icon-zhuye",
                            title:"角色管理",
                            show:true
                        },
                    },
    
                ]
            }
        ],
    },
    {
        path: '/login',
        name: 'login',
        component:()=>import("@/views/login/index.vue")
    },
]
const router = createRouter({
    history: createWebHashHistory(),
    routes,
  });
   
  export default router;