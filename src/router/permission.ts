import router from "./index"
import { useTagsStore } from '@/store/modules/tags';
import {useLoginStore} from "@/store/index.ts"
import NProgress from 'nprogress'
import 'nprogress/nprogress.css'
NProgress.configure({
    showSpinner: false
  })
//白名单，不进行拦截处理
const whiteList:Array<string> = ['/login', '/auth-redirect', '/bind', '/register'];


//路由拦截
router.beforeEach((to, from, next) => {
    console.log(from,"from")
    NProgress.start()
    if (sessionStorage.getItem('token')) {
        // to.meta.title && store.dispatch('setTitle', to.meta.title)
        /* has token*/
        if (to.path === '/login') {
            next({ path: '' })
            NProgress.done()
        } else {
            // console.log(useLoginStore,"useLoginStore")
            const UseLogin=useLoginStore()
            UseLogin.setLoginUserInfo()
            // if (store.getters.roles.length === 0) {
            //     // isRelogin.show = true
            //     // 判断当前用户是否已拉取完user_info信息
            //     store.dispatch('GetInfo').then(() => {
            //         // isRelogin.show = false
            //         store.dispatch('GenerateRoutes').then(accessRoutes => {
            //             // 根据roles权限生成可访问的路由表
            //             accessRoutes.forEach(route => {
            //                 if (!isHttp(route.path)) {
            //                     router.addRoute(route) // 动态添加可访问路由表
            //                 }
            //             })
            //             next({ ...to, replace: true }) // hack方法 确保addRoutes已完成
            //         })
            //     }).catch(err => {
            //     	//捕捉错误，退出登录
            //         store.dispatch('LogOut').then(() => {
            //             ElMessage.error(err)
            //             next({ path: '/' })
            //         })
            //     })
            // } else {
            //     next()
            // }
            next()
        }
    } else {
        // 没有token
        if (whiteList.indexOf(to.path) !== -1) {
            // 在免登录白名单，直接进入
            next()
        } else {
            next(`/login`) // 否则全部重定向到登录页
            NProgress.done()
        }
    }
})

router.afterEach((to) => {
    if (to.meta.title) {
        document.title = to.meta.title as string
    }
    NProgress.done(true)
    if (to.matched.length > 1) {
        const store=useTagsStore()
        store.addTag(to);
    }
})
export default router;