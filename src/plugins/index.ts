import { ElMessage } from 'element-plus'
import {useLoad} from "@/utils/saveLoad"
import MyDetail from '@/components/common/ MyDetail.vue'
import MySection from '@/components/common/MySection.vue'
import TextArea from '@/components/common/TextArea.vue'
import MyDrawerRight from '@/components/common/MyDrawerRight.vue'
export const  globalComponentPlugin={
    // 参数 会自动接收到安装本插件的应用实例
  install(app: App) {
     app.config.globalProperties.$message = ElMessage
     app.config.globalProperties.$useLoad=useLoad
     app.component('MyDetail',MyDetail)
     app.component('MySection',MySection)
     app.component('TextArea',TextArea)
     app.component('MyDrawerRight',MyDrawerRight)
  }


}