import { createApp } from 'vue'
import './style.css'
import App from './App.vue'
import router from "@/router"
import ElementPlus from 'element-plus'
import 'element-plus/dist/index.css'
import 'normalize.css/normalize.css'
import { createPinia } from 'pinia'
import piniaPluginPersistedstate from 'pinia-plugin-persistedstate'
import './assets/icon/iconfont.css'
import "@/router/permission"
import "@/styles/element.css"
import directives from './directives';
import {globalComponentPlugin} from "@/plugins"

const pinia = createPinia();
pinia.use(piniaPluginPersistedstate);

const app=createApp(App)
app.use(pinia)
app.use(directives);
// app.use(createPinia())
// 全局设置element-plus
app.use(ElementPlus)
app.use(globalComponentPlugin)
app.use(router)
app.mount('#app')
