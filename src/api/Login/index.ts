import axios from '@/utils/request'
import { LoginDto,LoginVo } from './types'
// 获取岗位列表
export const Login = (params: LoginDto) => {
    return axios.post<LoginVo>('/api//admin/employee/login', params);
} 
export const GetCaptcha = () => {
    return axios.get<string>('/api//admin/employee/getCaptcha');
} 