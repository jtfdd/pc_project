// 定义登录数据传输对象
export interface LoginDto {
  username: string;
  password: string;
  code:string
}

// 定义登录验证对象
export interface LoginVo {
  token: string;
}