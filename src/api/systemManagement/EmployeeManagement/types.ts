import {PageType} from "@/api/common/R"
export namespace employee{
    export interface Employee {
        id?: number|null;
        account: string;//账户
        nickname: string;
        email: string;
        avatarUrl: string;
        phone: string;
        roleId: number;
        roleName?: string;
        status: number;
        remark: string;
    };
    export interface EmployeePageDto extends PageType {
        account: string;//账户
        roleId: number|string;
        status: number|string;
    }
}