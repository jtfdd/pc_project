import axios from '@/utils/request'
import {employee} from "./types"
import {IdType} from "@/api/common/R"

// 新增员工
export const AddEmployee = (params: employee.Employee) => {
    return axios.post<employee.Employee>('/api/admin/employee/addEmployee',params);
} 
// 编辑员工
export const EditEmployee = (params: employee.Employee) => {
    return axios.post<employee.Employee>('/api/admin/employee/editEmployee',params);
} 
// 根据员工id获取详情
export const SelectEmployeeById = (params:IdType) => {
    return axios.get<employee.Employee>('/api/admin/employee/selectEmployeeById',params);
} 
// 员工列表
export const EmployeePage = (params: employee.EmployeePageDto) => {
    return axios.post<employee.Employee>('/api/admin/employee/employeePage',params);
} 
// 删除员工
export const DelEmployee = (params:IdType) => {
    return axios.post<employee.Employee>('/api/admin/employee/delEmployee',params);
} 
export const EmployeeInfo = () => {
    return axios.get<employee.Employee>('/api/admin/employee/employeeInfo');
} 
export const EmployeeMenu = () => {
    return axios.get<employee.Employee>('/api/admin/employee/employeeMenu');
} 

export const UpdateEmployeeStatus = (params:IdType) => {
    return axios.get<null>('/api/admin/employee/updateEmployeeStatus',params);
} 