import axios from '@/utils/request'
export namespace User {
  // 用户登录表单
  export interface UserDto {
    name?:string
    type?:string
    phone?:string
    password?:string
    avatarUrl?:string
    additionalInfo?:string
    roleId?:String
    status?:number|boolean|string
  }
  // 用户信息
  export interface UserVo {
    name:string
    status:number|boolean
    postCode:string
  }
  export interface UserId {
      id: number;
    }
    export interface StatusParams extends UserId {
          status: number;
    }
}
//用户分页列表
export const UserPage = (params: User.UserDto) => {
    return axios.post<User.UserVo>('/api/admin/employee/page', params);
} 
// 新增或保存
export const UserSave = (params: User.UserDto) => {
  return axios.post<User.UserVo>('/api/admin/employee/saveOrUpdate', params);
} 
// 更新状态
export const UpdateUserStatus = (params: User.StatusParams) => {
  return axios.post<User.UserVo>('/api/admin/employee/updateUserStatus', params);
} 
//删除用户
export const DelUser = (params: User.UserId) => {
  return axios.delete<User.UserVo>('/api/admin/employee/delUser', params);
} 
//获取用户信息
export const UserInfo = (params: User.UserId) => {
  return axios.post<User.UserVo>('/api/admin/employee/userInfo', params);
}