import axios from '@/utils/request'
import {Role} from "./types"
import {IdType} from "@/api/common/R"
// export namespace Role {
//   // 用户登录表单
//   export interface RoleDto {
//     roleName?:string
//     status?:number|boolean|string
//     remark?:string,
//     menuList?:number[]
//   }
//   // 用户信息
//   export interface UserVo {
//     name:string
//     status:number|boolean
//     postCode:string
//   }
//   export interface RoleId {
//       id: number
//       name?:string
//     }
//     export interface StatusParams extends RoleId {
//           status: number;
//     }
// }
//角色分页列表
export const RolePage = (params: Role.RolePageDto) => {
    return axios.post<Role.RoleItem>('/api/admin/role/rolePage', params);
} 
// 新增或保存
export const RoleSave = (params: Role.RoleItem) => {
  return axios.post<Role.RoleItem>('/api/admin/role/saveOrUpdataRole', params);
} 
// 更新状态
export const UpdateRoleStatus = (params: IdType) => {
  return axios.get<null>('/api/admin/role/updateRoleStatus', params);
} 
//删除角色
export const DelRole = (params:IdType) => {
  return axios.delete<null>('/api/admin/role/delRole', params);
} 
// //获取角色信息
export const RoleInfo = (params: IdType) => {
  return axios.get<Role.RoleItem>('/api/admin/role/roleInfo', params);
}
// //获取角色列表
// export const RoleList = (params: Role.RoleId) => {
//   return axios.get<Role.UserVo>('/api/admin/role/roleList', params);
// }
//模糊查询角色列表
export const RoleLikeList = (params: Role.RolePageDto) => {
  return axios.post<Role.RoleItem>('/api/admin/role/roleLikeList', params);
}