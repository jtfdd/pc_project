import {PageType} from "@/api/common/R"
export namespace Role{
    export interface RoleItem {
        id?: number|null;
        account: string;//账户
        nickname: string;
        email: string;
        avatarUrl: string;
        phone: string;
        roleId: number;
        roleName?: string;
        status: number;
        remark: string;
    };
    export interface RolePageDto extends PageType {
        roleName: number|string;
        status: number|string;
    }
}