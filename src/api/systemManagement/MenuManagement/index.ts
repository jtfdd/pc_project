import axios from '@/utils/request'
export namespace Job {
  // 用户登录表单
  export interface PageDto {
    name?:string
    parentId?:string|null|number
    compoents?:string
    icon?:string
    path?:string
    orderNumber?:string
    isShow?:string
    isActive?:string
    status?:string
    type?:string
  }
  // 登录成功后返回的token
  export interface LoginResData {
    name:string
    status:number|boolean
    postCode:string
  }
  export interface JObId {
      id: number;
    }
    export interface StatusParams extends JObId {
          status: number;
    }
}
// 获取岗位列表
export const MenuPage = (params: Job.PageDto) => {
    return axios.post<Job.PageDto>('/api/admin/menu/pageLIst', params);
} 
// 新增岗位
export const SaveOrUpdateMenu = (params: Job.PageDto) => {
     console.log(params)
    return axios.post<Job.PageDto>('/api/admin/menu/saveOrUpdataMenu', params);
}
// 删除岗位
export const DeleteByIdMenu = (params: Job.JObId) => {
    return axios.delete<Job.PageDto>('/api/admin/menu/delMenu', params);
}
// 根据id获取详情
export const GetByIdMenu = (params: Job.JObId) => {
    return axios.get<Job.PageDto>('/api/admin/menu/menuInfo', params);
}
// 根据id获取菜单子类
export const GetMenuSub = (params: Job.JObId) => {
  return axios.get<Job.PageDto>('/api/admin/menu/menuSub', params);
}
