// 分页
export interface PageResult<T>{
    total: number,
    list: T[]
}
export interface PageType{
    limit:number,
    page:number,
}
// 数据返回的接口
// 定义请求响应参数，不含data
interface Result {
  code: number;
  msg: string
}

// 请求响应参数，包含data
export interface ResultData<T> extends Result {
  data: T;
}
// 定义请求响应参数，不含data
export interface IdType {
  id: number;
}
