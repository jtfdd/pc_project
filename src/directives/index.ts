export default (app)=>{
    app.directive('debounceClick', {
        // 当被绑定的元素挂载到 DOM 中时……
        mounted(el,binding) {
            let timer = null;
            el.addEventListener('click',()=>{
                let firstClick = !timer;
                if(firstClick){
                  binding.value();
                }
                if(timer){
                    clearTimeout(timer);
                  }
                  timer = setTimeout(()=>{
                      timer = null;
                  },1000)
              })
            }
        })
    }          